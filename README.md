|Branch|Status|
|------|:--------:|
|master|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/sysctl/badges/master/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/sysctl/commits/master)
|develop|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/sysctl/badges/develop/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/sysctl/commits/develop)

## Ansible Role
### **_sysctl_**

Set or remove specified kernel variables on RHEL/CentOS, Debian/Ubuntu and Alpine Linux servers.

## Requirements

- Ansible 2.0 and higher

## Role Default Variables
```yaml
## Tier can be one of following:
##   * all
##   * group
##   * host
##   * play
sysctl_variables_<tier>:
  - key: net.ip_v4.forward      # The dot-separated key (aka 'path') specifying the sysctl variable.
    state: present              # Whether the entry should be 'present' or 'absent' in the sysctl file.
    value: 1                    # Desired value of the sysctl key.
    sysctl_set: True            # Verify token value with the 'sysctl' command and set with '-w' if necessary.
    reload: True                # If 'True', performs a '/sbin/sysctl -p' if the 'sysctl_file' is updated.
                                # If 'False', does not reload sysctl even if the 'sysctl'_file is updated.
    sysctl_file: myrole.conf    # Specifies the absolute path to sysctl confing file, if not default '/etc/sysctl.conf'.
    file_order: 90              # Set the file name prefix to define order which file will be processed in
```

## Dependencies

None.

## License

MIT / BSD

## Author Information

This role was created in 2018 by ITSupportMe, LLC.
